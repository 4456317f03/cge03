/*
 *      |------------------------------------------------------------------------------------------------------------+
 *      |                                             Copyright - Northest Missouri State University                  | 
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Team #04  : Sreelakshmi Onteddu, Naveen Kumar Puvvada                                                                                                                        | 
 *      |Unit #04  : Sreelakshmi Onteddu                                  | 
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Description: This program handles the user  details.                                                                 | 
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |    NAME                            VERSION                       CHANGES                                    |
 *      |--------------------------------------------------------------------------------------------------------------
 *      |    Sreelakshmi Onteddu                                      0.0.1                      change log
 *                                                                                              create empty user.js
 *                                                                                              Added required code
 *      +--------------------------------------------------------------------------------------------------------------
*/const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
 email: { type: String, unique: true },
 password: String,
 passwordResetToken: String,
 passwordResetExpires: Date,
 tokens: Array,
 profile: {
   name: String
 }
}, { timestamps: true })

/**
 * Password hash middleware.
 */
userSchema.pre('save', function save (next) {
  const user = this
  if (!user.isModified('password')) { return next() }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) { return next(err) }
    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) { return next(err) }
      user.password = hash
      next()
    })
  })
})

/**
 * Helper method for validating password.
 */
userSchema.methods.comparePassword = function comparePassword (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    cb(err, isMatch)
  })
}

module.exports = mongoose.model('User', userSchema)
