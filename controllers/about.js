/*
 *      |------------------------------------------------------------------------------------------------------------+
 *      |                                             Copyright - Northest Missouri State University                  | 
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Team #03  : Naveen Kumar Chandaluri, Hitesh Kolla                                                                                                                          | 
 *      |Unit #03  : Naveen Kumar Chandaluri                                         | 
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Description: This program hangles the about details of the each team members.                                                                 | 
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |    NAME                            VERSION                       CHANGES                                    |
 *      |--------------------------------------------------------------------------------------------------------------
 *      |    Naveen Kumar Chandaluri          0.0.1                      change log
 *      |                                     0.0.2                      Modified about pages for each team.
 *      |                                     0.0.3                      Handled each about pages for team members.
 *      |
 *      +--------------------------------------------------------------------------------------------------------------
*/

const express = require('express')
const api = express.Router()

// Specify the handler for each required combination of URI and HTTP verb

// HANDLE VIEW DISPLAY REQUESTS --------------------------------------------

// GET t1
api.get('/t1', (req, res) => {
 console.log(`Handling GET ${req}`)
 res.render('about/t1/index.ejs',
       { title: 'TeamName', layout: 'layout.ejs' })
})
api.get('/t1/a', (req, res) => {
 console.log(`Handling GET ${req}`)
 res.render('about/t1/a/index.ejs',
       { title: 'TeamMember1 Shirisha Reddy Rapole', layout: 'layout.ejs' })
})
api.get('/t1/b', (req, res) => {
 console.log(`Handling GET ${req}`)
 return res.render('about/t1/b/index.ejs',
       { title: 'TeamMember2 Hemanth Narne', layout: 'layout.ejs' })
})

// GET t2
api.get('/t2', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t2/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
     })
api.get('/t2/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t2/a/index.ejs',
            { title: 'TeamMember1 Sirisha Vinukonda', layout: 'layout.ejs' })
     })

api.get('/t2/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t2/b/index.ejs',
            { title: 'TeamMember2 Yeshwant Muppaneni', layout: 'layout.ejs' })
     })

     // GET t3
     api.get('/t3', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t3/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
     })
     
api.get('/t3/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t3/a/index.ejs',
            { title: 'TeamMember1 Naveen Kumar Chandaluri', layout: 'layout.ejs' })
     })
     api.get('/t3/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t3/b/index.ejs',
            { title: 'TeamMember2 Hitesh Kolla', layout: 'layout.ejs' })
     })

     // GET t6
     api.get('/t6', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t6/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
     })
     api.get('/t6/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t6/a/index.ejs',
            { title: 'TeamMember1 Chandrika Meda', layout: 'layout.ejs' })
     }) 
     api.get('/t6/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t6/b/index.ejs',
            { title: 'TeamMember2 Naresh Vunnam', layout: 'layout.ejs' })
     })

     // GET t4
     api.get('/t4', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t4/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
     })
     api.get('/t4/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t4/a/index.ejs',
            { title: 'TeamMember1 Sree Lakshmi', layout: 'layout.ejs' })
     })
     api.get('/t4/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      return res.render('about/t4/b/index.ejs',
            { title: 'TeamMember2 Naveen Kumar Puvvada', layout: 'layout.ejs' })
     })

     // GET t5
     api.get('/t5', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t5/index.ejs',
            { title: 'TeamName', layout: 'layout.ejs' })
     })
     api.get('/t5/a', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t5/a/index.ejs',
            { title: 'TeamMember5 Hema Chaudary Paladugu', layout: 'layout.ejs' })
     })
     api.get('/t5/b', (req, res) => {
      console.log(`Handling GET ${req}`)
      res.render('about/t5/b/index.ejs',
            { title: 'TeamMember5 Swathi Dasari', layout: 'layout.ejs' })
     })

// GET t2... through t12 here... and then finally:
module.exports = api