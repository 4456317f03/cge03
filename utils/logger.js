/**

 *

 *      +----------------------------------------------------------------------------------------------------------------+

 *      |                                             Copyright - Northest Missouri State University                  | 

*       |-----------------------------------------------------------------------------------------------------------------|

 *      |Team #  01                                                                                                                         | 

 *      |Unit # 07 : Hemanth,Narne, Shirisha Reddy,Rapole                                                                                         | 

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |Description: This program  generates the logs.                                                                   | 

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |    NAME                            VERSION                       CHANGES                                    |

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |    Hemanth,Narne                                      0.0.1(Initial)             /utils/ (logger.js, seeder.js)                               | 

 *      |                                                                                                                                       | 

 *      |                                                                                                                                       | 

 *      +-----------------------------------------------------------------------------------------------------------------+

 */
const winston = require('winston')
const fs = require('fs')
const path = require('path')

const logDir = 'logs'

if (!fs.existsSync(logDir)) { fs.mkdirSync(logDir)}

const logger = new (winston.Logger)({
 level: 'debug',
 transports: [
   new (winston.transports.Console)({ json: false, timestamp: true }),
   new winston.transports.File({ filename: path.join(logDir, '/debug.log'), json: false })
 ],
 exceptionHandlers: [
   new (winston.transports.Console)({ json: false, timestamp: true, humanReadableUnhandledException: true }),
   new winston.transports.File({ filename: path.join(logDir, '/debug.log'), json: false, humanReadableUnhandledException: true })
 ],
 exitOnError: false
})

module.exports = logger
