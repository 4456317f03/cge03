/**

 *

 *      +----------------------------------------------------------------------------------------------------------------+

 *      |                                             Copyright - Northest Missouri State University                  | 

*       |-----------------------------------------------------------------------------------------------------------------|

 *      |Team #   01                                                                                                                        | 

 *      |Unit # 07 : Hemanth,Narne , Shirisha Redyy Rapole                                                                                        | 

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |Description: This program does sedder part                                                                | 

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |    NAME                            VERSION                       CHANGES                                    |

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |    Hemanth,Narne                                      0.0.1(Initial)                      /utils/ (logger.js, seeder.js)                      | 

 *      |                                                                                                                                       | 

 *      |                                                                                                                                       | 

 *      +-----------------------------------------------------------------------------------------------------------------+

 */// set up a temporary (in memory) database
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')
const estimates = require('../data/estimates.json')

module.exports = (app) => {
 LOG.info('START seeder.')
 const db = new Datastore()
 db.loadDatabase()

 // insert the sample data into our data store
 db.insert(estimates)

 // initialize app.locals (these objects will be available to our controllers)
 app.locals.estimates = db.find(estimates)

 LOG.debug(`${estimates.length} estimates`)
 LOG.info('END Seeder. Sample data read and verified.')
}