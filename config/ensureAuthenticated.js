/*
 *      |------------------------------------------------------------------------------------------------------------+
 *      |                                             Copyright - Northest Missouri State University                  | 
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Team #03  : Naveen Kumar Puvvada, SreeLakshmi Onteddu                                                                                                                          | 
 *      |Unit #03  : Naveen Kumar Puvvada                                         | 
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |Description: This program handles Authentication.                                                                 | 
 *      |-----------------------------------------------------------------------------------------------------------------|
 *      |    NAME                            VERSION                       CHANGES                                    |
 *      |--------------------------------------------------------------------------------------------------------------
 *      |    Naveen Kumar Puvvada                                       0.0.1                      change log
 *      +--------------------------------------------------------------------------------------------------------------
*/
module.exports = {
    ensureLoggedIn (req, res, next) {
      if (req.isAuthenticated()) {
        return next()
      }
      req.flash('failure', 'Please login before creating an estimate.')
      res.render('account/login', { status: 'Please login.', errors: '' })
    }
  }