/**
 *
 *      +--------------------------------------------------------------------------------------------------------------------+
 *      |                                             Copyright - Northest Missouri State University                         | 
*       |--------------------------------------------------------------------------------------------------------------------|
 *      |Team 02                                                                                                             | 
 *      |Unit 08  : Sirisha vinukonda , Yeshwant Muppaneni                                                                   | 
 *      |--------------------------------------------------------------------------------------------------------------------|
 *      |Description: This program gives the estimate tests                                                                  | 
 *      |--------------------------------------------------------------------------------------------------------------------|
 *      |    NAME                            VERSION                       CHANGES                                           |
 *      |--------------------------------------------------------------------------------------------------------------------|
 *      |    Yeshwant                       0.0.1(Initial)              Added content from passport app                      | 
 *      |                                                                                                                    | 
 *      |                                                                                                                    | 
 *      +--------------------------------------------------------------------------------------------------------------------+
 */
process.env.NODE_ENV = 'test'
const app = require('../../app.js')
const LOG = require('../../utils/logger.js')
const mocha = require('mocha')
const expect = require('chai').expect
var request = require('supertest')
const EMAIL = 'test@test.com'
const PASSWORD = 'Password_1'
const path = 'estimate'

LOG.debug('Starting test/controllers/estimateTest.js.')

// set up user credentials to pass to the login method
const userCredentials = {
  email: EMAIL,
  password: PASSWORD
}

// login the user before running tests
var authenticatedUser = request.agent(app)

mocha.describe('API Tests - Estimate Controller - Not Authenticated', function () {
  mocha.describe('GET /estimate', function () {
    mocha.it('responds with status 302 (redirect) when not authenticated', function (done) {
      request(app)
        .get('/' + path)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.status).to.be.equal(302)
          done()
        })
    })
  })
  mocha.describe('GET /estimate/xyz', function () {
    mocha.it('responds with status 404', function (done) {
      request(app)
        .get('/' + path + '/xyz')
        .end(function (err, res) {
          if (err) { }
          expect(res.status).to.be.equal(404)
          done()
        })
    })
  })
})
mocha.describe('API Tests - Estimate Controller - Authenticated', function () {
  mocha.before(function (done) {
    authenticatedUser
      .post('/login')
      .send(userCredentials)
      .end(function (err, response) {
        if (err) { return done(err) }
        expect(response.statusCode).to.equal(302)
        expect('Location', '/')
        done()
      })
  })
  mocha.describe('GET /estimate', function () {
    mocha.it('responds with status 200 when authenticated', function (done) {
      request(app)
      authenticatedUser.get('/' + path)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.status).to.be.equal(200)
          done()
        })
    })
    mocha.it('should return index page', function (done) {
      request(app)
      authenticatedUser.get('/' + path)
        .end(function (err, res) {
          if (err) return done(err)
          expect('Content-type', 'text/html; charset=utf-8')
          expect('X-Powered-By', 'Express')
          expect(res.body.error).equal(undefined)
          done()
        })
    })
  })
  mocha.describe('GET /estimate/findall', function () {
    mocha.it('responds with status 200 when authenticated', function (done) {
      request(app)
      authenticatedUser.get('/' + path + '/findall')
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.status).to.be.equal(200)
          done()
        })
    })
    mocha.it('should return json', function (done) {
      request(app)
      authenticatedUser.get('/' + path + '/findall')
        .end(function (err, res) {
          if (err) return done(err)
          expect('Content-type', 'application/json; charset=utf-8')
          expect(res.body.error).equal(undefined)
          expect(res.body).not.equal(null)
          LOG.info('res.body = ', res.body)
          done()
        })
    })
  })
  mocha.describe('GET /estimate/findone/1', function () {
    mocha.it('responds with status 200 when authenticated', function (done) {
      request(app)
      authenticatedUser.get('/' + path + '/findone/1')
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.status).to.be.equal(200)
          done()
        })
    })
    mocha.it('should return json', function (done) {
      request(app)
      authenticatedUser.get('/' + path + '/findone/1')
        .end(function (err, res) {
          if (err) return done(err)
          expect('Content-type', 'application/json; charset=utf-8')
          expect(res.body.error).equal(undefined)
          expect(res.body).not.equal(null)
          LOG.info('res.body = ', res.body)
          done()
        })
    })
  })
})
